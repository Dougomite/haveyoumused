﻿using UnityEngine;
using System.Collections;

public class splashScreen : MonoBehaviour {

	bool internetReached = false;
	// Use this for initialization
	IEnumerator Start() {
		string HtmlText = GetHtmlFromUri("http://google.com");

		float internetWaitTime = 0;
		while(!internetReached)
		{
			internetWaitTime += Time.deltaTime;

			if(internetWaitTime > 10)
			{
				//TimeOut App
				MobileNativeMessage dialog = new MobileNativeMessage("Internet Unavailable", "Check your internet connection and try again.");
				dialog.OnComplete += CloseApp;
				yield break;
			}
			yield return null;
		}

		if(!internetReached)
		{
			yield break;
		}

		internetWaitTime = 0;
		while(InputNapkin.instance.currentTitle == null)
		{
			internetWaitTime += Time.deltaTime;

			if(internetWaitTime > 10)
			{
				//TimeOut App
				MobileNativeMessage dialog = new MobileNativeMessage("Poem Database Unavailable", "Check your internet connection and try again.");
				dialog.OnComplete += CloseApp;
				yield break;
			}

			yield return null;
		}
		this.gameObject.SetActive(false);
	
	}

	public void CloseApp()
	{
		Application.Quit();
	}

	public string GetHtmlFromUri(string resource)
	{
		string html = string.Empty;
		System.Net.HttpWebRequest req = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(resource);
		try
		{
			using (System.Net.HttpWebResponse resp = (System.Net.HttpWebResponse)req.GetResponse())
			{
				bool isSuccess = (int)resp.StatusCode < 299 && (int)resp.StatusCode >= 200;
				if (isSuccess)
				{
					internetReached = true;
				}
			}
		}
		catch
		{
			return "";
		}
		return html;
	}

}
