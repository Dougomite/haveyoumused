//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright � 2011-2013 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// Very basic script that will activate or deactivate an object (and all of its children) when clicked.
/// </summary>

[AddComponentMenu("NGUI/Interaction/Button Activate Multiple")]
public class UIButtonActivateMultiple : MonoBehaviour
{
    public GameObject[] targets;
    public bool state = true;

    void OnClick () { if (targets != null) foreach(GameObject target in targets){NGUITools.SetActive(target, state);} }
}