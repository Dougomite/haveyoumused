﻿using UnityEngine;
using System.Collections;

public class PreviousTitle : MonoBehaviour {

	public UILabel previousTitleLabel;
	public int thisTitleIndex;

	void OnClick()
	{
		PreviousTitleMenu.instance.gameObject.SetActive(false);
		OutputNapkin.instance.gameObject.SetActive(true);
		if(thisTitleIndex >= 0)
			OutputNapkin.instance.GetTitleFromDatabase(thisTitleIndex);
		else
			OutputNapkin.instance.GetTodayTitleFromDatabase();


	}

}
