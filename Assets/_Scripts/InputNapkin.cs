﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;

public class InputNapkin : MonoBehaviour {

	public static InputNapkin instance;
	public OutputNapkin outputNapkin;

	[SerializeField]
	public UILabel titleText, poemText, titleInputText;
	public GameObject SubmitTitleButton, SubmitPoemButton;

	int titleIndex = 0;
	public ParseObject currentTitle = null;

	public string titleToLoad, poemToLoad;


	void Awake()
	{
		if(instance == null)
			instance = this;

#if MOBILE
		TouchScreenKeyboard.hideInput = true;
#endif

		if(outputNapkin != null)
		{
			OutputNapkin.instance = outputNapkin;
		}

		string urlLink=null;
		#if UNITY_IOS
		urlLink = PlayerPrefs.GetString("url","");
		#elif UNITY_ANDROID
		urlLink = AndroidDeepLink.GetURL();
		#endif
		if(!string.IsNullOrEmpty(urlLink) && urlLink != "null" )
		{
			Debug.Log(urlLink);
			urlLink.Replace("hym://","");
			urlLink.Replace("poems/","");
			urlLink.Replace("p1=","");
			urlLink.Replace("p2=","");
			urlLink.Replace("&","");
			string[] profiles = urlLink.Split("-".ToCharArray());
			if(profiles.Length > 1)
			{
				titleToLoad = profiles[0];
				poemToLoad = profiles[1];
				if(profiles.Length > 2 && bool.Parse(profiles[2]))
					OutputNapkin.instance.previousTitleMenu.userTitles = true;
					
			}
			PlayerPrefs.SetString("url","");
		}
	}

	void Start()
	{
		GetTodayTitleFromDatabase();
	}

	void GetTodayTitleFromDatabase()
	{
			titleIndex = -1;
			StartCoroutine(GetTitleASync());
	}

	void GetTitleFromDatabase(int index = -1)
	{
		if(index >= 0)
		{
			titleIndex = index;
			StartCoroutine(GetTitleASync());
		}
		else
			StartCoroutine(GetRandomTitleASync());
	}

	IEnumerator GetTitleASync()
	{
		var query = ParseObject.GetQuery("Title");
		
		var t2 = query.WhereEqualTo("index",titleIndex).FirstAsync();
		
		while(!t2.IsCompleted)
		{
			yield return 0;
		}

		if(t2.IsFaulted)
		{
			Debug.Log(t2.Exception);
			yield break;
		}

		currentTitle = t2.Result;
		titleText.text = t2.Result.Get<string>("text");
		//Choose random font
		if(FontList.Instance.mTrueTypeFonts.Count > 0)
		{
			titleText.trueTypeFont = FontList.Instance.mTrueTypeFonts[Random.Range(0, FontList.Instance.mTrueTypeFonts.Count)];
			poemText.trueTypeFont = FontList.Instance.mTrueTypeFonts[Random.Range(0, FontList.Instance.mTrueTypeFonts.Count)];
		}
		titleText.gameObject.SetActive(true);
		poemText.gameObject.SetActive(true);
		SubmitPoemButton.SetActive(true);

		if(PlayerPrefs.GetString("DailyTitleComplete","") == currentTitle.ObjectId)
		{
			//Poem completed for today
			this.gameObject.SetActive(false);
			OutputNapkin.instance.gameObject.SetActive(true);
		}
	}

	IEnumerator GetRandomTitleASync()
	{
		var query = ParseObject.GetQuery("Title");
		int count = 0;
		var t = query.CountAsync();
		while(!t.IsCompleted)
		{
			yield return 0;
		}


		if(t.IsFaulted)
		{
			Debug.Log(t.Exception);
			yield break;
		}

		count = t.Result;

		var t2 = query.WhereEqualTo("index",Random.Range(0,count)).FirstAsync();

		while(!t2.IsCompleted)
		{
			yield return 0;
		}

		if(t2.IsFaulted)
		{
			Debug.Log(t2.Exception);
			yield break;
		}

		currentTitle = t2.Result;
		titleText.text = t2.Result.Get<string>("text");
		//Choose random font
		if(FontList.Instance.mTrueTypeFonts.Count > 0)
		{
			titleText.trueTypeFont = FontList.Instance.mTrueTypeFonts[Random.Range(0, FontList.Instance.mTrueTypeFonts.Count)];
			poemText.trueTypeFont = FontList.Instance.mTrueTypeFonts[Random.Range(0, FontList.Instance.mTrueTypeFonts.Count)];
		}
		titleText.gameObject.SetActive(true);
		poemText.gameObject.SetActive(true);
	}

	void SubmitPoem()
	{
		StartCoroutine(SubmitPoemASync());
	}

	IEnumerator SubmitPoemASync()
	{
		if(titleText.text == "Type Your Poem Here" || string.IsNullOrEmpty(titleText.text))
		{
			yield break;
		}

		bool saveDone = false;
		var poemList = currentTitle.GetRelation<ParseObject>("poems");
		var poem = new ParseObject("Poem");
		poem["titleText"] = titleText.text;
		poem["text"] = poemText.text;
		poem["upVote"] = 0;
		poem["downVote"] = 0;
		poem["rating"] = 0.2f;
		poem.SaveAsync().ContinueWith(task =>
        {
				if(task.Exception != null)
				{
					Debug.Log(task.Exception);
					return;
				}

			poemList.Add(poem);
			currentTitle.Increment("numberOfPoems");
			currentTitle.SaveAsync().ContinueWith(t =>
			{
				saveDone = true;
				if(task.Exception != null)
				{
					Debug.Log(task.Exception);
					return;
				}
			});
			Debug.Log ("Poem Submitted");
		});

		while(!saveDone)
		{
			yield return null;
		}
	
		//PlayerPrefs.SetString("DailyTitleComplete", currentTitle.ObjectId);

		this.gameObject.SetActive(false);
		OutputNapkin.instance.gameObject.SetActive(true);

        MobileNativeMessage mnp = new MobileNativeMessage("Submitted", "Poem Submitted for the title - " + titleText.text);
	}

	public void ShowSubmitPoem(ParseObject poemTitle)
	{
		if(poemTitle == null)
			return;

		currentTitle = poemTitle;
		titleText.text = currentTitle.Get<string>("text");
		OutputNapkin.instance.gameObject.SetActive(false);
		InputNapkin.instance.gameObject.SetActive(true);
		//titleInputText.gameObject.SetActive(true);
		//titleText.gameObject.SetActive(true);
		SubmitPoemButton.SetActive(true);
		SubmitTitleButton.SetActive(false);
	}

	public void ShowSubmitTitle()
	{
		OutputNapkin.instance.gameObject.SetActive(false);
		InputNapkin.instance.gameObject.SetActive(true);
		//titleInputText.gameObject.SetActive(true);
		//titleText.gameObject.SetActive(false);
		SubmitPoemButton.SetActive(false);
		SubmitTitleButton.SetActive(true);
	}


	void SubmitTitle()
	{
		StartCoroutine(SubmitTitleASync());
	}

	IEnumerator SubmitTitleASync()
	{
		if(titleInputText.text == "Type Your Poem Here" || string.IsNullOrEmpty(titleInputText.text))
		{
			yield break;
		}

		var query = ParseObject.GetQuery("TitleSubmission");
		int count = 0;
		var t = query.CountAsync();
		while(!t.IsCompleted)
		{
			yield return 0;
		}
		
		
		if(t.IsFaulted)
		{
			Debug.Log(t.Exception);
			yield break;
		}
		
		count = t.Result;

		var title = new ParseObject("TitleSubmission");
		title["text"] = titleInputText.text;
		title["index"] = count;
		title["approved"] = false;
		title["numberOfPoems"] = 0;
		title.SaveAsync();
		Debug.Log ("Title Submitted");

		OutputNapkin.instance.gameObject.SetActive(false);
		InputNapkin.instance.gameObject.SetActive(false);
		PreviousTitleMenu.instance.gameObject.SetActive(true);

        MobileNativeMessage mnp = new MobileNativeMessage("Submitted", "Title Submitted");
    }
	
}
