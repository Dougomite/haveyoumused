﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;

public class PreviousTitleMenu : MonoBehaviour {

	public static PreviousTitleMenu instance;
	public GameObject previousTitleTemplate;
	public UIGrid titlesGrid;
	IList<ParseObject> titlesList;
	public UIScrollView scrollView;

	public bool userTitles = false;

	void Awake()
	{
		if(instance == null)
			instance = this;

	}

	void OnEnable()
	{
//		if(titlesList == null)
//			StartCoroutine(GrabAllTitles());
	}

	public void GetUserTitles()
	{
		this.gameObject.SetActive(true);
		userTitles = true;
		StartCoroutine(GrabAllTitles());
	}

	public void GetTitles()
	{
		this.gameObject.SetActive(true);
		userTitles = false;
		StartCoroutine(GrabAllTitles());
	}

	IEnumerator GrabAllTitles()
	{
		MNP.ShowPreloader("", "");
		ParseQuery<ParseObject> query;
		if(userTitles)
			query = ParseObject.GetQuery("UserTitles");
		else
			query = ParseObject.GetQuery("Title");
		
		var t = query.WhereGreaterThanOrEqualTo("index",-1).FindAsync();
		while(!t.IsCompleted)
		{
			yield return 0;
		}
		
		
		if(t.IsFaulted)
		{
			Debug.Log(t.Exception);
			yield break;
		}

		titlesList = new List<ParseObject>(t.Result);

		ShowTitles();
		MNP.HidePreloader();
	}

	void ShowTitles()
	{


		foreach(Transform go in titlesGrid.GetChildList())
		{   
			if(go != null)
			{
				DestroyImmediate (go.gameObject);
			}
		}
		int i = 0;
		foreach(ParseObject po in titlesList)
		{
			if(po == null || po.Get<int>("index") == -2)
			{
				continue;
			}
			PreviousTitle title = NGUITools.AddChild(titlesGrid.gameObject, previousTitleTemplate).GetComponent<PreviousTitle>();
			title.gameObject.name = i.ToString();
			title.previousTitleLabel.text = po.Get<string>("text") + "(" + po.Get<int>("numberOfPoems") + ")";
			title.thisTitleIndex = po.Get<int>("index");

			title.gameObject.transform.parent = titlesGrid.transform;
			title.gameObject.SetActive(true);
			i++;
		}

		titlesGrid.Reposition();
		scrollView.ResetPosition ();
	}

	public void SortByAlphabet()
	{
		List<ParseObject> sortedPoems = new List<ParseObject>(titlesList);
		sortedPoems.Sort(new SortParseByName());
		titlesList = sortedPoems;
		ShowTitles();
	}

	public void SortByDate()
	{
		//currentPoems
		List<ParseObject> sortedPoems = new List<ParseObject>(titlesList);
		sortedPoems.Sort(new SortParseByDate());
		titlesList = sortedPoems;
		ShowTitles();
	}

	public void SortByPopularity()
	{
		List<ParseObject> sortedPoems = new List<ParseObject>(titlesList);
		sortedPoems.Sort(new SortParseByPopularity());
		titlesList = sortedPoems;
		ShowTitles();
	}
	class SortParseByDate : IComparer<ParseObject>
	{

		public int Compare(ParseObject po1, ParseObject po2)
		{
			if(po1.CreatedAt > po2.CreatedAt)
				return 1;
			else
				return -1;
		}
	}

	class SortParseByPopularity : IComparer<ParseObject>
	{

		public int Compare(ParseObject po1, ParseObject po2)
		{
			if(po1.Get<int>("numberOfPoems") > po1.Get<int>("numberOfPoems"))
				return 1;
			else
				return -1;
		}
	}

	class SortParseByName : IComparer<ParseObject>
	{

		public int Compare(ParseObject po1, ParseObject po2)
		{
            string compare1 = po1.Get<string>("text").ToLower();
            string compare2 = po2.Get<string>("text").ToLower();

            if (compare1.StartsWith("the "))
            {
                compare1 = compare1.Remove(0,4);
            }
            if (compare2.StartsWith("the "))
            {
                compare2 = compare2.Remove(0, 4);
            }

            if (compare1.StartsWith("a "))
            {
                compare1 = compare1.Remove(0, 2);
            }
            if (compare2.StartsWith("a "))
            {
                compare2 = compare2.Remove(0, 2);
            }


            return compare1.CompareTo(compare2);
		}
	}


}
