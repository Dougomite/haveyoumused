﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine(RunAds());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public IEnumerator RunAds()
	{
		UM_AdManager.Init();
		while(!UM_AdManager.IsInited)
		{
			yield return null;
		}

		yield return new WaitForSeconds(1f);
		int bannerId1 = UM_AdManager.CreateAdBanner(TextAnchor.LowerCenter);

		float AdTime = 0;
		bool playingAds = true;
		while(playingAds == true)
		{
			yield return 0;
			AdTime -= Time.deltaTime;

			if(AdTime <= 0)
			{
				if(UM_AdManager.IsBannerOnScreen(bannerId1))
				{
					UM_AdManager.RefreshBanner(bannerId1);
					UM_AdManager.ShowBanner(bannerId1);
				}
				else
				{
					//Debug.Log("Showing banner #" + bannerId1);
					UM_AdManager.ShowBanner(bannerId1);

					yield return new WaitForSeconds(25f);
					{
						UM_AdManager.HideBanner(bannerId1);
					}
				}

				AdTime = 60;
			}
		}
	}
}
