﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FontList : MonoBehaviour {

	public List<Font> mTrueTypeFonts;

	private static FontList instance;
	
	public static FontList Instance {
		get
		{
			if(instance == null)
				instance = GameObject.FindObjectOfType(typeof(FontList)) as FontList;
			
			return instance;
		}	
	}

}
