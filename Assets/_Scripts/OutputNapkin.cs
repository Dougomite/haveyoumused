﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Parse;

public class OutputNapkin : MonoBehaviour {

	const string linkURL = "https://deeplink.me/haveyouent.wix.com/haveyoumused/";
	public static OutputNapkin instance;
	
	[SerializeField]
	public UILabel titleText, poemText;
	
	int titleIndex = 0;
	ParseObject currentTitle;
	IList <ParseObject> currentPoems;

	public UILabel currentPoemNumber, TotalPoemsNumber;
	public UIScrollView scrollingPanel;

	public GameObject napkinObject;
    public UIButton rateUpButton, rateDownButton;

    public List<ParseObject> objectsToSave = new List<ParseObject> ();
	int saveInterval = 5;
	public float saveTimer = 0;

	public int poemCount = 0;
	public int refreshTime = 60;
	public GameObject AdPopup;
	public GameObject AdOK;
    int adCount = 1;


    public PreviousTitleMenu previousTitleMenu;

	public static GameObject shareObjectWindow;

	void Awake()
	{
		if(instance == null)
			instance = this;
		Init();
	}

	IEnumerator Start()
	{
		if(!string.IsNullOrEmpty(InputNapkin.instance.titleToLoad) )
		{
			MNP.ShowPreloader("", "");

			ParseQuery<ParseObject> query;
			if(previousTitleMenu.userTitles)
				query = ParseObject.GetQuery("UserTitles");
			else
				query = ParseObject.GetQuery("Title");
			
			var t = query.GetAsync(InputNapkin.instance.titleToLoad);
			
			while(!t.IsCompleted)
			{
				yield return null;
			}

			MNP.HidePreloader();

			if(t.Exception != null)
			{
				Debug.Log(t.Exception);
				GetTodayTitleFromDatabase();
			}
			else{
				currentTitle = t.Result;
				titleText.text = currentTitle.Get<string>("text");
			}
			napkinObject.transform.localPosition = new Vector3(850,-52,0);

			if(!this.gameObject.activeSelf)
				this.gameObject.SetActive(true);

			if(!this.gameObject.activeInHierarchy)
				this.transform.parent.gameObject.SetActive(true);
			
			GetPoems(InputNapkin.instance.poemToLoad);

			yield break;
		}


		GetTodayTitleFromDatabase();

		//this.gameObject.SetActive(false);
	}

	void OnEnable()
	{
		poemText.MarkAsChanged();
	}

	public void Init()
	{
		if(instance == null)
			instance = this;
		
		Parse.ParseConfig.CurrentConfig.TryGetValue<int>("AdsRefreshTime", out refreshTime);
		StartCoroutine(InitASync());
		StartCoroutine(SaveObjects());
	}

	IEnumerator InitASync()
	{
		var query = ParseObject.GetQuery("Poem");
		var t = query.CountAsync();
		while(!t.IsCompleted)
		{
			yield return 0;
		}
	}

	void FixedUpdate()
	{
		if(poemCount > 10 * adCount && !AdPopup.activeInHierarchy)
		{
			long lastTime = System.Convert.ToInt64(PlayerPrefs.GetString ("AdTime", "0"));
			var dt = System.DateTime.FromBinary(lastTime);
			//var dt3 = System.DateTime.FromFileTimeUtc (lastTime);
			var dt2 = System.DateTime.UtcNow;
			var timeSpan = (System.DateTime.UtcNow - dt);
			if (refreshTime > timeSpan.TotalSeconds ) 
			{
				return;
			}

            adCount++;

            AdPopup.SetActive(true);
			AdOK.SetActive(true);
		}
	}

	public IEnumerator SlideOutNapkin()
	{
		float dist = 0f;
		Vector3 startPosition = new Vector3(0,-52,0);
		while(dist < 1f)
		{
			dist += Time.deltaTime * 8f;
			napkinObject.transform.localPosition = Vector3.Lerp(startPosition, new Vector3(-850,-52,0),dist);
			yield return 0;
		}

		yield return new WaitForSeconds(0.2f);
		StartCoroutine(SlideInNapkin());
	}

	public IEnumerator SlideInNapkin()
	{
		float dist = 0f;
		Vector3 startPosition = new Vector3(850,-52,0);
		while(dist < 1f)
		{
			dist += Time.deltaTime * 8f;
			napkinObject.transform.localPosition = Vector3.Lerp(startPosition, new Vector3(-0,-52,0),dist);
	
			yield return 0;
		}
		poemText.MarkAsChanged();
	}
	
	public void GetTodayTitleFromDatabase()
	{
		previousTitleMenu.userTitles = false;
		napkinObject.transform.localPosition = new Vector3(850,-52,0);
		if(!this.gameObject.activeSelf)
			this.gameObject.SetActive(true);
		
		if(!this.gameObject.activeInHierarchy)
			this.transform.parent.gameObject.SetActive(true);

		titleIndex = -1;
		StartCoroutine(GetTitleASync());
	}

	void GetTitleFromDatabase(GameObject go)
	{
		GetTitleFromDatabase();
	}

	public void GetTitleFromDatabase(int index = -1)
	{
		napkinObject.transform.localPosition = new Vector3(850,-52,0);

		if(!this.gameObject.activeSelf)
			this.gameObject.SetActive(true);

		if(!this.gameObject.activeInHierarchy)
			this.transform.parent.gameObject.SetActive(true);

		if(index >= 0)
		{
			titleIndex = index;
			StartCoroutine(GetTitleASync());
		}
		else
			StartCoroutine(GetRandomTitleASync());
	}
	
	IEnumerator GetTitleASync()
	{
		MNP.ShowPreloader("", "");

		ParseQuery<ParseObject> query;
		if(previousTitleMenu.userTitles)
			query = ParseObject.GetQuery("UserTitles");
		else
			query = ParseObject.GetQuery("Title");
		
		var t2 = query.WhereEqualTo("index",titleIndex).FirstAsync();
		
		while(!t2.IsCompleted)
		{
			yield return 0;
		}

		MNP.HidePreloader();

		if(t2.IsFaulted)
		{
			Debug.Log(t2.Exception);
			yield break;
		}
		
		currentTitle = t2.Result;
		StartCoroutine(GetPoems());
	}
	
	IEnumerator GetRandomTitleASync()
	{
		MNP.ShowPreloader("", "");

		ParseQuery<ParseObject> query;
		if(previousTitleMenu.userTitles)
			query = ParseObject.GetQuery("UserTitles");
		else
			query = ParseObject.GetQuery("Title");
		
		int count = 0;
		var t = query.CountAsync();
		while(!t.IsCompleted)
		{
			yield return 0;
		}
		
		MNP.HidePreloader();

		if(t.IsFaulted)
		{
			Debug.Log(t.Exception);
			yield break;
		}
		
		count = t.Result;
		
		var t2 = query.WhereEqualTo("index",Random.Range(-1,count-1)).FirstAsync();
		
		while(!t2.IsCompleted)
		{
			yield return 0;
		}
		
		if(t2.IsFaulted)
		{
			Debug.Log(t2.Exception);
			yield break;
		}
		
		currentTitle = t2.Result;
		StartCoroutine(GetPoems());
	}

	IEnumerator GetPoems(string poemToLoad = null)
	{
		MNP.ShowPreloader("", "");

		while(currentTitle == null)
		{
			yield return 0;
		}

		var poemList = currentTitle.GetRelation<ParseObject>("poems");
		var t = poemList.Query.WhereGreaterThanOrEqualTo("rating",0.2f).FindAsync();//.WhereEqualTo("index", Random.Range(0,currentTitle.Get<double>("numberOfPoems")));

		while(!t.IsCompleted)
		{
			yield return 0;
		}

		MNP.HidePreloader();

		if(t.IsFaulted)
		{
			Debug.Log(t.Exception);
			yield break;
		}


		currentPoems = new List<ParseObject>(t.Result);
		int chosenPoem = 0;
		if(poemToLoad == null)
		{
			chosenPoem = Random.Range(0,currentPoems.Count);
		}
		else
		{
			for(int i = 0; i < currentPoems.Count; i++)
			{
				if(currentPoems[i].ObjectId == poemToLoad)
				{
					chosenPoem = i;
					break;
				}
			}
		}

		currentPoemNumber.text = (chosenPoem + 1).ToString();
		TotalPoemsNumber.text = currentPoems.Count.ToString();


		scrollingPanel.ResetPosition();
		titleText.text = currentTitle.Get<string>("text");
		poemText.text = currentPoems[chosenPoem].Get<string>("text");
		poemText.ResizeCollider();
		poemText.MarkAsChanged();
		scrollingPanel.ResetPosition();
		//scrollingPanel.clipRange = new Vector4(0,0, scrollingPanel.clipRange.z, scrollingPanel.clipRange.w);

		//Choose random font
		if(FontList.Instance.mTrueTypeFonts.Count > 0)
		{
			titleText.trueTypeFont = FontList.Instance.mTrueTypeFonts[Random.Range(0, FontList.Instance.mTrueTypeFonts.Count)];
			poemText.trueTypeFont = FontList.Instance.mTrueTypeFonts[Random.Range(0, FontList.Instance.mTrueTypeFonts.Count)];
		}
		titleText.gameObject.SetActive(true);
		poemText.gameObject.SetActive(true);
		StartCoroutine(SlideInNapkin());
	}

	public void RandomPoemInTitle()
	{
		StartCoroutine(SlideOutNapkin());
		int chosenPoem = Random.Range(0,currentPoems.Count - 1);
        ShowPoem(chosenPoem);
    }

	void FirstPoemInTitle()
	{
		StartCoroutine(SlideOutNapkin());
		int chosenPoem = 0;

        ShowPoem(chosenPoem);
    }

	void NextPoemInTitle()
	{
		StartCoroutine(SlideOutNapkin());
		int chosenPoem = int.Parse(currentPoemNumber.text);
		if(chosenPoem >= currentPoems.Count)
			chosenPoem = 0;

        ShowPoem(chosenPoem);
    }

    void PrevPoemInTitle()
    {
        StartCoroutine(SlideOutNapkin());
        int chosenPoem = int.Parse(currentPoemNumber.text) - 2;
        if (chosenPoem < 0)
            chosenPoem = currentPoems.Count - 1;

        ShowPoem(chosenPoem);
    }

    void ShowPoem(int chosenPoem)
    {
        currentPoemNumber.text = (chosenPoem + 1).ToString();
        TotalPoemsNumber.text = currentPoems.Count.ToString();

        scrollingPanel.ResetPosition();
        poemText.text = currentPoems[chosenPoem].Get<string>("text");
        poemText.ResizeCollider();
        poemText.MarkAsChanged();
        scrollingPanel.ResetPosition();
        //scrollingPanel.clipRange = new Vector4(0,0, scrollingPanel.clipRange.z, scrollingPanel.clipRange.w);

        //Choose random font
        if (FontList.Instance.mTrueTypeFonts.Count > 0)
        {
            //titleText.trueTypeFont = FontList.Instance.mTrueTypeFonts[Random.Range(0, FontList.Instance.mTrueTypeFonts.Count)];
            poemText.trueTypeFont = FontList.Instance.mTrueTypeFonts[Random.Range(0, FontList.Instance.mTrueTypeFonts.Count)];
        }

        poemCount++;
        rateDownButton.enabled = true;
        rateUpButton.enabled = true;
    }

	void SortByDate()
	{
		//currentPoems
		List<ParseObject> sortedPoems = new List<ParseObject>(currentPoems);
		sortedPoems.Sort(new SortParseByDate());
		currentPoems = sortedPoems;
		FirstPoemInTitle();
	}

	void SortByPopularity()
	{
		List<ParseObject> sortedPoems = new List<ParseObject>(currentPoems);
		sortedPoems.Sort(new SortParseByPopularity());
		currentPoems = sortedPoems;
		FirstPoemInTitle();
    }

	void OpenSponsor()
	{
		//Application.OpenURL("www.barfly.com");
	}

	public void RateUp()
	{
			RatePoem(true);
	}

	public void RateDown()
	{
			RatePoem(false);
	}

	void RatePoem(bool rateUp)
	{
		ParseObject p = currentPoems[int.Parse(currentPoemNumber.text)];
		if(rateUp)
			p["upVote"] = p.Get<int>("upVote") + 1;
		else
			p["downVote"] =  p.Get<int>("downVote") + 1;
	
		if( p.Get<int>("downVote") + p.Get<int>("upVote") > 10)
			p["rating"] = Rating(p.Get<int>("upVote"), p.Get<int>("downVote"));

        rateDownButton.enabled = false;
        rateUpButton.enabled = false;

        SaveObject(p);

		//NextPoemInTitle();
	}

	void ShowSubmitPoem()
	{
		InputNapkin.instance.ShowSubmitPoem( currentTitle );
	}

	public void ShowInterstitialAd()
	{
		if(!UM_AdManager.IsInited)
		{
			UM_AdManager.Init();
			InterstitialAdComplete();
			return;
		}
			
		PlayerPrefs.SetString ("AdTime", System.DateTime.UtcNow.ToBinary().ToString());
		UM_AdManager.OnInterstitialClosed += InterstitialAdComplete;
		UM_AdManager.OnInterstitialLoadFail += InterstitialAdComplete;
		UM_AdManager.OnInterstitialLoaded += InterstitialAdLoaded;

		UM_AdManager.LoadInterstitialAd();
		AdOK.SetActive(false);

		#if UNITY_EDITOR
		InterstitialAdComplete();
		#endif
	}

	public void InterstitialAdComplete()
	{
		UM_AdManager.ResetActions();
		poemCount = 0;
		AdPopup.SetActive(false);
	}

	public void InterstitialAdLoaded()
	{
		UM_AdManager.ShowInterstitialAd();
		poemCount = 0;
	}

	public void ReportPoem()
	{
		MobileNativeDialog dialog = new MobileNativeDialog("Report Poem", "Are you sure you want to Report & Block this poem?");
		dialog.OnComplete += ReportPoemDone;
	}

	void ReportPoemDone(MNDialogResult result)
	{
		switch(result) {
		case MNDialogResult.YES:
			ParseObject p = currentPoems[int.Parse(currentPoemNumber.text)];
			p["downVote"] =  p.Get<int>("downVote") + 5;

			if( p.Get<int>("downVote") + p.Get<int>("upVote") > 10)
				p["rating"] = Rating(p.Get<int>("upVote"), p.Get<int>("downVote"));

			SaveObject(p);

			currentPoems.RemoveAt(int.Parse(currentPoemNumber.text));
			NextPoemInTitle();
			break;
		case MNDialogResult.NO:
			break;
		default:
			break;
		}
		
	}

	public void SharePoem()
	{
		int chosenPoem = int.Parse(currentPoemNumber.text) - 1 ;
		if(chosenPoem >= currentPoems.Count)
			chosenPoem = 0;
		UM_ShareUtility.ShareMedia("Have You Mused poem:\n", titleText.text + "\n\n" + poemText.text + "\n\n" + linkURL + currentTitle.ObjectId +"-"+currentPoems[chosenPoem].ObjectId + "-" + previousTitleMenu.userTitles.ToString() ,null );
		//shareObjectWindow.gameObject.SetActive(false);
		//mainProfilesObject.SetActive(true);
		GoogleAnalytics.Client.CreateHit(GoogleAnalyticsHitType.EVENT);
		GoogleAnalytics.Client.SetEventCategory("profile");
		GoogleAnalytics.Client.SetEventAction("shareSearch");
		GoogleAnalytics.Client.Send();
	}

	public void FBSharePoem()
	{
		int chosenPoem = int.Parse(currentPoemNumber.text) - 1;

		UM_ShareUtility.FacebookShare("Have You Mused poem:\n" + titleText.text + "\n\n" + poemText.text + "\n\n" + linkURL + currentTitle.ObjectId +"-"+currentPoems[chosenPoem].ObjectId + "-" + previousTitleMenu.userTitles.ToString() );
		//shareObjectWindow.gameObject.SetActive(false);
		//mainProfilesObject.SetActive(true);
		GoogleAnalytics.Client.CreateHit(GoogleAnalyticsHitType.EVENT);
		GoogleAnalytics.Client.SetEventCategory("profile");
		GoogleAnalytics.Client.SetEventAction("shareFB");
		GoogleAnalytics.Client.Send();
	}

	public void TWTSharePoem()
	{
		int chosenPoem = int.Parse(currentPoemNumber.text) - 1;
		if(chosenPoem >= currentPoems.Count)
			chosenPoem = 0;
		UM_ShareUtility.TwitterShare("Have You Mused poem:\n" + titleText.text + "\n\n" + poemText.text + "\n\n" + linkURL + currentTitle.ObjectId +"-"+currentPoems[chosenPoem].ObjectId + "-" + previousTitleMenu.userTitles.ToString() );
		//shareObjectWindow.gameObject.SetActive(false);
		//mainProfilesObject.SetActive(true);
		GoogleAnalytics.Client.CreateHit(GoogleAnalyticsHitType.EVENT);
		GoogleAnalytics.Client.SetEventCategory("profile");
		GoogleAnalytics.Client.SetEventAction("shareTWT");
		GoogleAnalytics.Client.Send();
	}

	public void SaveObject(ParseObject p_Object)
	{
		objectsToSave.Add (p_Object);
	}

	public IEnumerator SaveObjects()
	{
		saveTimer = saveInterval;
		while(true)
		{
			if (objectsToSave.Count > 0) {
				saveTimer -= Time.unscaledDeltaTime;
				if (saveTimer <= 0) {
					saveTimer = saveInterval;

					for(int i = 0; i < objectsToSave.Count; i++)
					{
						objectsToSave[i] = objectsToSave[i];
						yield return null;
					}

					var task = objectsToSave.SaveAllAsync ();

					while (!task.IsCompleted) {
						yield return null;
					}

					if (task.Exception != null) {
						Debug.LogWarning ("Object Save Failed \n" + task.Exception);
						continue;
					}
					else
					{
						Debug.Log (objectsToSave.Count + " changes saved.");
						objectsToSave.Clear ();
					}
					Resources.UnloadUnusedAssets();
				}
			}
			yield return null;
		}
	}


	class SortParseByDate : IComparer<ParseObject>
	{

		public int Compare(ParseObject po1, ParseObject po2)
		{
			if(po1.CreatedAt > po2.CreatedAt)
				return 1;
			else
				return -1;
		}
	}

	class SortParseByPopularity : IComparer<ParseObject>
	{
		
		public int Compare(ParseObject po1, ParseObject po2)
		{
			if(po1.Get<float>("rating") > po1.Get<float>("rating"))
				return 1;
			else
				return -1;
		}
	}

	public static double Rating(int positive, int negative)
	{
			return (((positive + 1.9208) / (positive + negative) - 1.96 * Mathf.Sqrt(((positive * negative) / (positive + negative)) + 0.9604f) / (positive + negative)) / (1f + 3.8416f / (positive + negative)));
	}
}
